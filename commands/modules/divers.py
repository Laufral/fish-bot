from discord.ext import commands
from classes.janken import *
import discord, logging, common, re, random, asyncio


class DiversCog(commands.Cog):
    """Commandes diverses que tout le monde peux utiliser."""

    def __init__(self, bot):
        self.bot = bot
        self.logger = common.get_newfilelogger(self.bot.options['LOGGERS']['divers_logname'],
                                               self.bot.options['LOGGERS']['divers_filename'],
                                               level=logging.INFO,
                                               console=True)
        self.logger.info("Instanciation du Cog 'DiversCog'")
        self.janken_game = None

    def cog_unload(self):
        self.logger.info("Fermeture du Cog 'DiversCog'")
        common.close_logger(self.logger)

    @commands.command(aliases=['beers'], hidden=False)
    async def beer(self, ctx, *mentions):
        """Sers une bière à soi ou aux @mentions.
		Aliases:
			/beer
			/beers

		Parametres:
			(mentions): Nom/Mention/ID d'un ou plusieurs membres discord.
				default: Sers un bière à l'autheur de la commande.

		Exemple:
			/beer Ikya Kaginor
			/beers
		"""
        members = ''
        nbr = 0
        for elem in mentions:
            member = common.search_member(ctx.guild, elem)
            if member is not None and member != ctx.author:
                members = members + member.mention + ' '
                nbr += 1
        if nbr < 1:
            result = f':beer: Voici votre bière {ctx.author.mention} :beer:'
        elif nbr > 1:
            result = f':beers:{ctx.author.mention} offre une tournée de bières à {members}:beers:'
        else:
            result = f":beers:{ctx.author.mention} t'offre une bière {members}:beers:"
        await ctx.send(result)
        await ctx.message.delete()
        common.log_command(self.logger, ctx, result)

    @commands.command(hidden=False)
    async def roll(self, ctx, *arg):
        """Lance des dés.
        Aliases:
            /roll

        Parametres:
            arg: Un lancé de dé ET/OU un calcul simple.

        Exemple:
            /roll 18+1d20+7+3d6-2
            /roll 18+30-(3*4)
        """
        regex = re.compile(r'((?:\d+)?(?:[dD](?:[\d]+))+)')
        regex2 = re.compile(r'^`?\(*[\d]+(?:(?:[()]*[+\-*/])[()]*(?:[\d]+))*\)*`?$')
        math = ''
        for elem in arg:
            math += elem.lower()
        math = math.replace('`', '')
        calc = math
        group = re.findall(regex, math)
        group2 = regex2.match(math)
        if group:
            for elem in group:
                stroll = self.rolling(elem)
                if stroll is None:
                    raise commands.BadArgument(f"'**{math}**' n'est pas un lancé de dé correcte.")
                calc = calc.replace(elem, f'({stroll})')
            group2 = regex2.match(calc.replace('**', ''))
        if group2 is not None:
            if calc == math:
                result = f'{ctx.author.mention} `{math}` = {eval(group2[0])}'
            else:
                result = f":game_die: {ctx.author.mention} `{math}` = {calc} = {self.calcul(calc.replace('**', ''))}"
            common.log_command(self.logger, ctx, result)
            await ctx.channel.send(result)
        else:
            info_error = f"'**{math}**' n'est pas un lancé de dé correcte."
            common.log_command(self.logger, ctx, info_error)
            raise commands.BadArgument(info_error)

    def rolling(self, group):
        rolls = []
        group = group.split('d')
        if len(group) > 2:
            return None
        stroll = ''
        i = 0
        while i < (1 if group[0] == '' else int(group[0])):
            rolls.append(random.randrange(int(group[1])) + 1)
            if rolls[i] == int(group[1]) or rolls[i] == 1:
                stroll = stroll + '**' + str(rolls[i]) + '**' + '+'
            else:
                stroll = stroll + str(rolls[i]) + '+'
            i += 1
        return stroll[:-1]

    def calcul(self, calc):
        result = eval(calc)
        if result < 1:
            return 1
        return result

    @commands.command(aliases=['toss'], hidden=False)
    async def flip(self, ctx, nombre: int = 1):
        """Fait un pile ou face.
        Aliases:
            /flip
            /toss

        Parametres:
            (nombre): Nombre de pièces à lancer.
                default: 1 pièce.

        Exemple:
            /flip 3
            /toss
        """
        # Verification parametres
        if nombre < 1:
            info = f"On ne peux pas lancer '**{nombre}**' pièces."
            await ctx.channel.send(info)
            common.log_command_error(self.logger, ctx, info)
            raise commands.BadArgument(info)
        # Initialisations variables
        result = ''
        pile = 0
        face = 0
        i = nombre
        # Tirages
        while i > 0:
            if random.randrange(2) == 1:
                pile += 1
                result += '**Pile** / '
            else:
                face += 1
                result += '**Face** / '
            i -= 1
        # Creation du résultat
        if nombre == 1:
            result = f':coin: {ctx.author.mention} lance une pièce: {result[0:-3]}'
        else:
            if pile > face:
                result = f':coin: {ctx.author.mention} lance {nombre} pièces: **Pile** gagne !\n{result[0:-3]}'
            elif pile < face:
                result = f':coin: {ctx.author.mention} lance {nombre} pièces: **Face** gagne !\n{result[0:-3]}'
            else:
                result = f':coin: {ctx.author.mention} lance {nombre} pièces: **Egalité** !\n{result[0:-3]}'
        common.log_command(self.logger, ctx, result)
        await ctx.channel.send(result)

    @commands.command(aliases=['shifumi'], hidden=False)
    async def janken(self, ctx, member = None):
        """Joue a pierre papier ciseaux avec un autres membre.
        Aliases:
            /janken
            /shifumi

        Parametres:
            membre: Nom/Mention/ID du second joueur.

        Exemple:
            /janken Ikya
            /shifumi Ikya
        """
        # Check sur le membre donné en parametre
        member_ = member
        member = common.search_member(ctx.guild, member)
        info_error = ''
        if member is None:
            info_error = f"L'utilisateur **{member_}** n'est pas membre de la guide {ctx.guild.name}"
        elif member.id == self.bot.user.id :
            info_error = "Vous ne pouvez pas jouer avec le bot"
        elif member == ctx.author:
            info_error = "Vous devez jouez avec quelqu'un d'autre que vous-même"
        if info_error != '':
            common.log_command_error(self.logger, ctx, info_error)
            await ctx.channel.send(info_error)
            raise commands.BadArgument(info_error)

        # Préparation au lancement du jeu
        msg = await ctx.send(
            f'**{ctx.author.display_name}** défie **{member.display_name}** au pierre papier ciseaux !')
        janken_game = Janken(self._janken_getreaction)
        janken_ctx = JankenContext(ctx.author.name, member.name)
        janken_ctx.discord_author = ctx.author
        janken_ctx.discord_player = member
        janken_ctx.timeout = 60
        # Lancement du jeu
        try:
            hand_author, hand_player, winner = await janken_game.process_game(janken_ctx)
        # Traitement des evenements, erreurs
        except JankenPlayerCancelsException as e:
            info = f"Le joueur '{e.player} à refuser de jouer au jeu"
            common.log_command(self.logger, ctx, info)
            await ctx.channel.send(info)
        except JankenBadHandException as e:
            # we provided a wrong hand to the game via getreaction - should not happen
            info = f"Le joueur '{e.player} à triché d'une manière ou d'une autre, le vilain"
            common.log_command_error(self.logger, ctx, info, f"hand: {e.hand}")
            await ctx.channel.send(info)
        except Exception as e:
            info = "An error has occured " + str(e)
            common.log_command_error(self.logger, ctx, info)
            await ctx.channel.send(info)

        else:
            # game went well
            reactions = ['❌', '✊', '✋', '✌']
            if winner is None:
                # equals
                chan_message = f'{ctx.author.mention} et {member.mention} ont fait une **égalité** sur pierre papier ' \
                               f'ciseaux!\n**{ctx.author.display_name}** a joué {reactions[hand_author.value]}\n' \
                               f'**{member.display_name}** a joué {reactions[hand_player.value]}'
                author_message = f'**Egalité** !!\n{msg.jump_url}'
                player_message = author_message
            elif winner == janken_ctx.name_author:
                # winner = autheur
                chan_message = f'{ctx.author.mention} gagne un pierre papier ciseaux contre {member.mention}!\n' \
                               f'**{ctx.author.display_name}** a joué {reactions[hand_author.value]}\n' \
                               f'**{member.display_name}** a joué {reactions[hand_player.value]}'
                author_message = f'Vous avez **gagné** ! :+1:\n{msg.jump_url}'
                player_message = f'Vous avez **perdu** ! :-1:\n{msg.jump_url}'

            else:
                # winner = player
                chan_message = f'{member.mention} gagne un pierre papier ciseaux contre {ctx.author.mention}!\n' \
                               f'**{ctx.author.display_name}** a joué {reactions[hand_author.value]}\n' \
                               f'**{member.display_name}** a joué {reactions[hand_player.value]}'
                author_message = f'Vous avez **perdu** ! :-1:\n{msg.jump_url}'
                player_message = f'Vous avez **gagné** ! :+1:\n{msg.jump_url}'
            await ctx.channel.send(chan_message)
            await ctx.author.send(author_message)
            await member.send(player_message)
            common.log_command(self.logger, ctx, f"{chan_message}\n\t{author_message}\n\t{player_message}")
            # await msg.delete()

    async def _janken_getreaction(self, ctx):
        # Envoi en dm du message et ajout d'une reaction de chaque sur celui-ci
        reactions = ['✊', '✋', '✌', '❌']
        reactions_ = ['❌', '✊', '✋', '✌']
        if ctx.target == ctx.name_author:
            member = ctx.discord_author
            # reactions = reactions[:-1] # Empecher de refuser
        else:
            member = ctx.discord_player

        message_jeu = await member.send(ctx.message)
        for reaction in reactions:
            await message_jeu.add_reaction(reaction)

        # Attente de la réponse - check valid reponse
        def check(reaction, user):
            return reaction.message == message_jeu and reaction.emoji in reactions and user != self.bot.user

        # Attente de la réponse - boucle d'attente
        cond = True
        while cond:
            try:
                react, user = await self.bot.wait_for('reaction_add', timeout=ctx.timeout, check=check)
            except asyncio.TimeoutError:
                cond = False
            # Attente de la réponse - reaction recue
            else:
                return JankenHand.parsevaluefromlistindex(reactions_, react.emoji)
        return None

    """@commands.command(hidden=False)
    async def anonyme(self, ctx, member: discord.Member = None, *, message: str = None):
        Envoie un MP annonymement à un membre.
		Aliases:
			/anonyme

		Parametres:
			membre: Nom/Mention/ID d'un membre discord.
			message: Le message à transmettre.

		Exemple:
			/anonyme Ikya T'es un super DEV ;-)
        if member is not None and message is not None:
            await ctx.message.delete()
            await member.send(f'Un anonyme vous envoie ce message:\n'
                              f'----------------------------------------\n{message}\n----------------------------------------')
        else:
            raise commands.BadArgument('Vous devez écrire un message.')
		"""

    # noinspection PyTypeChecker


def setup(bot):
    bot.add_cog(DiversCog(bot))
