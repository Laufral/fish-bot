from discord.ext import commands
import discord, logging, common

class OwnerCog(commands.Cog):
    """Les actions avec le propriétaire du bot"""

    def __init__(self, bot):
        self.bot = bot
        # Chargement du logger
        self.logger = common.get_newfilelogger(self.bot.options['LOGGERS']['owner_logname'],
                                               self.bot.options['LOGGERS']['owner_filename'],
                                               level=logging.INFO,
                                               console=True)
        self.logger.info("Instanciation du Cog 'OwnerCog'")

    def cog_unload(self):
        self.logger.info("Fermeture du Cog 'OwnerCog'")
        common.close_logger(self.logger)

    @commands.Cog.listener()
    async def on_ready(self):
        bot_owner = await self.bot.application_info()
        self.bot_owner = bot_owner.owner
        self.logger.info('Notification au propriétaire du bot du démarage du serveur')
        await self.bot_owner.send(
            "```fix\nMy power is at your service!\nName = "+self.bot.user.name+"\n"
            "ID = " + str(self.bot.user.id) + "\n```")

def setup(bot):
    bot.add_cog(OwnerCog(bot))
