from discord.ext import commands
import discord, common, logging, asyncio

class StreamCog(commands.Cog):
    """Les actions avec le propriétaire du bot"""

    def __init__(self, bot):
        self.bot = bot
        # Chargement du logger
        self.logger = common.get_newfilelogger(self.bot.options['LOGGERS']['owner_logname'],
                                               self.bot.options['LOGGERS']['owner_filename'],
                                               level=logging.INFO,
                                               console=True)
        self.logger.info("Instanciation du Cog 'StreamCog'")
        self.bot.stream_guilds_publishstream_lock = asyncio.Lock()
        self.bot.stream_guilds_publishstream = {}

    @commands.Cog.listener()
    async def on_ready(self):
        async with self.bot.stream_guilds_publishstream_lock:
            for guild in self.bot.guilds:
                self.bot.stream_guilds_publishstream[str(guild.id)] = True
        print(self.bot.stream_guilds_publishstream)

    @commands.Cog.listener()
    async def on_member_update(self, before, after):
        if after.bot:
            return
        if before.nick != after.nick:
            print("nick event")
            return
        before.roles.sort()
        after.roles.sort()
        if before.status != after.status:
            print("status event")
            return
        if before.roles != after.roles:
            print("role event")
            return
        if before.pending != after.pending:
            print("pending event")
            return
        if after.activity is None:
            return
        if after.activity.type != discord.ActivityType.streaming:
            return
        if after.activity.url is None:
            return

        publish = False
        for role in after.roles:
            if role.name.lower() == "streamer":
                publish = True
                break
        if not publish:
            print("not publish")
            return

        publish = False
        for guild in self.bot.guilds:
            for channel in guild.text_channels:
                if channel.name.lower() == "stream" and self.bot.stream_guilds_publishstream[str(guild.id)]:
                    publish = True
                    await channel.send(after.activity.url)
                    common.log_event(self.logger,"on_member_update", after, f"Stream published: {after.activity.url}")

        if not publish:
            print("not publish 2")
            return




    @commands.check_any(common.in_adminlist())
    @commands.group(name='stream', hidden=False)
    async def stream_base(self, ctx):
        if ctx.invoked_subcommand is not None:
            return
        await ctx.channel.send(f"```\n{ctx.command.help}\n```")
        common.log_command(self.logger, ctx, ctx.command.help)

    @commands.check_any(common.in_adminlist())
    @stream_base.command(name='enable', hidden=False)
    async def stream_enable_publishstream(self, ctx):
        async with self.bot.stream_guilds_publishstream_lock:
            self.bot.stream_guilds_publishstream[str(ctx.guild.id)] = True
        result = f"Activation de la publication des stream"
        await ctx.channel.send(result)
        common.log_command(self.logger, ctx, result+f"\n\tGuild: {ctx.guild}")

    @commands.check_any(common.in_adminlist())
    @stream_base.command(name='disable', hidden=False)
    async def stream_disable_publishstream(self, ctx):
        async with self.bot.stream_guilds_publishstream_lock:
            self.bot.stream_guilds_publishstream[str(ctx.guild.id)] = False
        result = f"Désactivation de la publication des stream"
        await ctx.channel.send(result)
        common.log_command(self.logger, ctx, result+f"\n\tGuild: {ctx.guild}")

    @commands.check_any(common.in_adminlist())
    @stream_base.command(name='state', hidden=False)
    async def stream_state_publishstream(self, ctx):
        print(self.bot.stream_guilds_publishstream)
        if self.bot.stream_guilds_publishstream[str(ctx.guild.id)]:
            result = f"Publication des stream pour ce discord est activé"
        else:
            result = f"Publication des stream pour ce discord est désactivé"
        await ctx.channel.send(result)
        common.log_command(self.logger, ctx, result+f"\n\tGuild: {ctx.guild}")

    def cog_unload(self):
        self.logger.info("Fermeture du Cog 'OwnerCog'")
        common.close_logger(self.logger)


def setup(bot):
    bot.add_cog(StreamCog(bot))