from discord.ext import commands
import discord, logging, common

class DevCog(commands.Cog):
    """Les actions avec le propriétaire du bot"""

    def __init__(self, bot):
        self.bot = bot
        # Chargement du logger
        self.logger = common.get_newfilelogger(self.bot.options['LOGGERS']['dev_logname'],
                                               self.bot.options['LOGGERS']['dev_filename'],
                                               level=logging.INFO,
                                               console=True)
        self.logger.info("Instanciation dun Cog 'DevCog'")

    def cog_unload(self):
        self.logger.info("Fermeture du Cog 'DevCog'")
        common.close_logger(self.logger)


def setup(bot):
    bot.add_cog(DevCog(bot))