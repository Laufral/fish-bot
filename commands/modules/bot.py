from discord.ext import commands
import discord, common, logging, asyncio, random

class BotCog(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        # Chargement du logger
        self.logger = common.get_newfilelogger(self.bot.options['LOGGERS']['bot_logname'],
                                               self.bot.options['LOGGERS']['bot_filename'],
                                               level=logging.INFO,
                                               console=True)
        self.precense_messages = []
        with open(self.bot.options["LIST"]["presence_messages"], 'r', encoding='utf-8') as fp:
            for cnt, line in enumerate(fp):
                self.precense_messages.append(str(line))

        self.presence_timeout = 900
        self.presence_running = False
        self.presence_task = asyncio.ensure_future(self._presence())
        self.logger.info("Instanciation du Cog 'BotCog'")

    def cog_unload(self):
        self.presence_task.cancel()
        self.presence_running = False
        self.logger.info("Fermeture du Cog 'BotCog'")
        common.close_logger(self.logger)

    @commands.command(hidden=False)
    async def uptime(self, ctx):
        """Montre depuis combien de temps le bot est en ligne.
		Aliases:
			/uptime

		Exemple:
			/uptime
		"""
        uptime = await self.bot.uptime()
        await ctx.send(f'My uptime is {uptime}')
        common.log_command(self.logger, ctx, f'My uptime is {uptime}')

    async def _presence(self):
        if not self.presence_running:
            self.presence_running = True
            await self._presence_set_randomactivity()
        await asyncio.sleep(self.presence_timeout)
        self.logger.info(f"Changement de status automatique")
        await self._presence_set_randomactivity()
        self.presence_task = asyncio.ensure_future(self._presence())

    @commands.group(hidden=False)
    async def presence(self, ctx):
        """Permet au travers de sous commandes, d'intéragir avec l'activité du bot et le système de changements automatique d'activités
           Affiche le status courant du bot si la commande est utilisée sans sous-commande.
        Aliases:
            /presence

        Sous-commande:
            (set): Permet, au travers de sous commandes, de paramétrer le comportement de l'activité du bot et du système de changements automatique d'activités
            (get): Permet, au travers de sous commandes, d'afficher des informations sur l'activité du bot et le système de changements automatique d'activités
            (show): Permet, au travers de sous commandes, d'afficher des informations sur l'activité du bot et le système de changements automatique d'activités

        Exemple:
            /presence"""
        if ctx.invoked_subcommand is not None:
            return
        await self._presence_get_activity(ctx)

    @commands.check_any(common.in_adminlist())
    @presence.group(name="get", aliases=['show'], hidden=False)
    async def presence_get(self, ctx):
        """Permet, au travers de sous commandes, d'afficher des informations sur l'activité du bot et le système de changements automatique d'activités
        Aliases:
            /presence get
            /presence show

        Sous-commandes:
            (activity): Permet d'afficher l'activité courante
            (timeout): Permet d'afficher le temps entre deux changements automatique d'activités
            (state): Permet d'afficher l'état du système de changements automatique d'activités

        Exemple:
            /presence get activity
            /presence get timeout
            /presence get state"""

        if ctx.invoked_subcommand is not None:
            return
        await ctx.channel.send(f"```\n{ctx.command.help}\n```")
        common.log_command(self.logger, ctx, ctx.command.help)

    @commands.check_any(common.in_adminlist())
    @presence_get.command(name="activity", hidden=False)
    async def presence_get_activity(self, ctx):
        """Permet d'afficher l'activité courante.
        Aliases:
            /presence get activity
            /presence show activity

        Exemple:
            /presence get activity"""
        await self._presence_get_activity(ctx)

    async def _presence_get_activity(self, ctx):
        if self.bot.activity is None:
            await ctx.message.add_reaction(u'\U0001F534')
            common.log_command_error(self.logger, ctx, "L'activité du bot est vide")
            raise commands.CommandError("L'activité du bot est vide")
        elif type(self.bot.activity) == discord.Activity:
            if self.bot.activity.type == discord.ActivityType.listening:
                result = f"J'écoute {self.bot.activity.name}"
            elif self.bot.activity.type == discord.ActivityType.playing:
                result = f"Je joue à {self.bot.activity.name}"
            elif self.bot.activity.type == discord.ActivityType.streaming:
                result = f"Je stream {self.bot.activity.name} à l'adresse {self.bot.activity.url}"
            elif self.bot.activity.type == discord.ActivityType.watching:
                result = f"Je regarde {self.bot.activity.name}"
            elif self.bot.activity.type == discord.ActivityType.competing:
                result = f"Je compète sur {self.bot.activity.name}"
            else:
                result = f"Je ne sais pas ce que je fais"
        elif type(self.bot.activity) == discord.Game:
            result = f"Je joue à {self.bot.activity.name}"
        elif type(self.bot.activity) == discord.Streaming:
                result = f"Je stream {self.bot.activity.name} à l'adresse {self.bot.activity.url}"
        else:
            result = f"Je ne sais pas ce que je fais"
        await ctx.channel.send(result)
        common.log_command(self.logger, ctx, result)

    @commands.check_any(common.in_adminlist())
    @presence_get.command(name="timeout", hidden=False)
    async def presence_get_timeout(self, ctx):
        """Permet d'afficher le temps entre deux changements automatique d'activités.
        Aliases:
            /presence get timeout
            /presence show timeout

        Exemple:
            /presence get timeout"""
        await ctx.channel.send(f"Timeout: {self.presence_timeout}")
        common.log_command(self.logger, ctx, f"Timeout: {self.presence_timeout}")

    @commands.check_any(common.in_adminlist())
    @presence_get.command(name="state", hidden=False)
    async def presence_get_state(self, ctx):
        """Permet d'afficher l'état du système de changements automatique d'activités.
        Aliases:
            /presence get state
            /presence show state

        Exemple:
            /presence get state"""
        if self.presence_running:
            result = "En marche"
        else:
            result = "Arreté"
        await ctx.channel.send(f"Etat du changement de présence automatique: {result}")
        common.log_command(self.logger, ctx, f"Etat du changement de présence automatique: {result}")

    @commands.check_any(common.in_adminlist())
    @presence.group(name="set", hidden=False)
    async def presence_set(self, ctx):
        """Permet, au travers de sous commandes, de paramétrer le comportement de l'activité du bot et du système de changements automatique d'activités.
        Aliases:
            /presence set

        Sous-commandes:
            (activity): Permet de changer l'activité courante
            (enable): Permet d'activer le changement d'activité automatique
            (disable): Permet de désactiver le changement d'activité automatique
            (timeout): Permet de changer le changement

        Exemple:
            /presence set activity playing 'Video Games'
            /presence set enable
            /presence set timeout 200"""

        if ctx.invoked_subcommand is not None:
            return
        await ctx.channel.send(f"```\n{ctx.command.help}\n```")
        common.log_command(self.logger, ctx, ctx.command.help)


    @commands.check_any(common.in_adminlist())
    @presence_set.command(name="activity", hidden=False)
    async def presence_set_activity(self, ctx, type: str, message: str, url: str = ''):
        """Permet de changer l'activité courante
        Aliases:
            /presence set activity

        Paramètres:
            (type): playing, listening, streaming, watching, competing
            (message): message à diffuser en activitée
            (url): url du stream si le type est streaming

        Exemple:
            /presence set activity listening Radio
            /presence set activity playing 'Video Games'
            /presence set activity streaming 'Sleeping only' www.twitch.com/
        """
        if type is None:
            await ctx.message.add_reaction(u'\U0001F534')
            common.log_command_error(self.logger, ctx, "Paramètre 'type' requis")
            raise commands.MissingRequiredArgument("Paramètre 'type' requis")
        if message is None:
            await ctx.message.add_reaction(u'\U0001F534')
            common.log_command_error(self.logger, ctx, "Paramètre 'message' requis")
            raise commands.MissingRequiredArgument("Paramètre 'message' requis")
        activity = common.getNewActivityFromString(type, message, url)
        if activity.type == discord.ActivityType.unknown:
            await ctx.message.add_reaction(u'\U0001F534')
            common.log_command_error(self.logger, ctx, f"Activitée {type} inconnue")
            raise commands.BadArgument(f"Activitée {type} inconnue")
        await self._presence_set_activity(activity)
        await ctx.message.add_reaction(u'\U0001F7E2')

    @commands.check_any(common.in_adminlist())
    @presence_set.command(name="enable", hidden=False)
    async def presence_set_enable(self, ctx):
        """Permet d'activer le changement d'activité automatique
        Aliases:
            /presence set enable

        Exemple:
            /presence set enable"""
        if not self.presence_running:
            self.presence_running = True
            self.presence_task = asyncio.ensure_future(self._presence(self.presence_timeout))
            common.log_command(self.logger, ctx, "Demande de changement d'activité automatique: démarré")
        else:
            common.log_command(self.logger, ctx, "Demande de changement d'activité automatique: déjà démarré")
        await ctx.message.add_reaction(u'\U0001F7E2')

    @commands.check_any(common.in_adminlist())
    @presence_set.command(name="disable", hidden=False)
    async def presence_set_disable(self, ctx):
        """Permet désactiver le changement d'activité automatique
        Aliases:
            /presence set disable

        Exemple:
            /presence set disable"""
        if self.presence_running:
            self.presence_task.cancel()
            self.presence_running = False
            common.log_command(self.logger, ctx, "Demande de changement d'activité automatique: stoppé")
        else:
            common.log_command(self.logger, ctx, "Demande de changement d'activité automatique: déjà stoppé")
        await ctx.message.add_reaction(u'\U0001F7E2')

    @commands.check_any(common.in_adminlist())
    @presence_set.command(name="timeout", hidden=False)
    async def presence_set_timeout(self, ctx, temps):
        """Permet de changer le temps d'attente entre deux changements d'activités
        Aliases:
            /presence set timeout

        Paramètres:
            (temps): le temps, en secondes, d'attente entre deux changements d'activités

        Exemple:
            /presence set timeout 200"""

        if temps is None:
            await ctx.message.add_reaction(u'\U0001F534')
            await ctx.channel.send("Le parametre temps doit être renseigné")
            common.log_command_error(self.logger, ctx, "Le parametres temps est manquant")
            raise commands.BadMissingArgument("Le parametres temps est manquant")

        try:
            self.presence_timeout = int(temps)
        except Exception as e:
            await ctx.message.add_reaction(u'\U0001F534')
            await ctx.channel.send("Le parametre temps doit être renseigné comme un nombre exprimé en secondes")
            common.log_command_error(self.logger, ctx, "Le parametres temps n'est pas renseigner comme un nombre")
            raise commands.BadArgument("Le parametres temps n'est pas renseigner comme un nombre")

        reloaded = False
        if self.presence_running:
            self.presence_task.cancel()
            self.presence_task = asyncio.ensure_future(self._presence())
            reloaded = True

        await ctx.message.add_reaction(u'\U0001F7E2')
        common.log_command(self.logger, ctx, f"Changement de timeout effectué (reloaded {reloaded}) nouvelle valeur: {temps}")


    @commands.check_any(common.in_adminlist())
    @presence_set.command(name="randomactivity", hidden=False)
    async def presence_set_randomactivity(self, ctx):
        common.log_command(self.logger, ctx, f"Changement de status random")
        await self._presence_set_randomactivity()
        await ctx.message.add_reaction(u'\U0001F7E2')

    async def _presence_set_randomactivity(self):
        status = random.choice(self.precense_messages)
        status = status.split('|')
        try:
            activity = common.getNewActivityFromString(status[0], status[1], status[2])
        except Exception:
            activity = common.getNewActivityFromString(status[0], status[1])
        await self._presence_set_activity(activity)

    async def _presence_set_activity(self, activity):
        self.bot.activity = activity
        await self.bot.change_presence(activity=activity)
        self.logger.info(f"Nouveau de status: {activity}")

def setup(bot):
    bot.add_cog(BotCog(bot))