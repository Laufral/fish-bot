from discord.ext import commands
from typing import Union
import discord, common, logging


class AdminCog(commands.Cog):
    """Commandes réservées aux Admins du bot."""

    def __init__(self, bot):
        self.bot = bot
        # Chargement du logger
        self.logger = common.get_newfilelogger(self.bot.options['LOGGERS']['admin_logname'],
                                               self.bot.options['LOGGERS']['admin_filename'],
                                               level=logging.INFO,
                                               console=True)
        self.logger.info("Instanciation dun Cog 'AdminCog'")

    def cog_unload(self):
        self.logger.info("Fermeture du Cog 'AdminCog'")
        common.close_logger(self.logger)

    @commands.check_any(common.in_adminlist())
    @commands.command(hidden=False)
    async def load(self, ctx, *, module):
        """Loads a module.
		Aliases:
			/load

		Parametres:
			module: Le nom d'un module.

		Exemple:
			/load admin
		"""
        try:
            self.bot.load_extension('commands.modules.{}'.format(module.lower()))
            await ctx.message.add_reaction(u'\U0001F7E2')
            common.log_command(self.logger, ctx, f"Chargement du module commands.module.{module.lower()}")
        except Exception as e:
            await ctx.message.add_reaction(u'\U0001F534')
            common.log_command_error(self.logger, ctx, f"Echec de chargement du module commands.module.{module.lower()}", e)
            raise commands.BadArgument(str(e))

    @commands.check_any(common.in_adminlist())
    @commands.command(hidden=False)
    async def unload(self, ctx, *, module):
        """Unloads a module.
		Aliases:
			/unload

		Parametres:
			module: Le nom d'un module.

		Exemple:
			/unload admin
		"""
        try:
            self.bot.unload_extension('commands.modules.{}'.format(module.lower()))
            await ctx.message.add_reaction(u'\U0001F7E2')
            common.log_command(self.logger, ctx, f"Déchargement du module commands.module.{module.lower()}")
        except Exception as e:
            await ctx.message.add_reaction(u'\U0001F534')
            common.log_command_error(self.logger, ctx, f"Echec de déchargement du module commands.module.{module.lower()}", e)
            raise commands.BadArgument(str(e))

    @commands.check_any(common.in_adminlist())
    @commands.command(name='reload', hidden=False)
    async def _reload(self, ctx, *modules):
        """Reloads one or more modules.
		Aliases:
			/reload

		Parametres:
			(modules): Le nom d'un ou plusieurs module(s).
				default: 'all'

		Exemple:
			/reload admin
		"""

        if not modules or modules[0] == 'all':
            modules = self.bot.modules

        charged_modules = []
        last_charged_module = ''
        last_tobecharged_module = ''
        try:
            for module in modules:
                last_tobecharged_module = module
                self.bot.reload_extension('commands.modules.{}'.format(module.lower()))
                charged_modules.append(module)
            common.log_command(self.logger, ctx, f"Rechargement des modules commands.module.{', commands.module.'.join(modules).lower()}")
            await ctx.channel.send(f"Rechargement des modules commands.module.{', commands.module.'.join(modules).lower()}")
        except Exception as e:
            if len(charged_modules) > 1:
                message =f"Erreur lors du chargement du module {last_tobecharged_module}\n\t" \
                         f"Modules chargés: commands.module.{', commands.module.'.join(charged_modules).lower()}"
            else:
                message = f"Erreur lors du chargement du module {last_tobecharged_module}\n\t"
            await ctx.channel.send(message)
            common.log_command_error(self.logger, ctx, message, e)
            #TODO, pas toujours un mauvais argument, peux être amélioré
            raise commands.BadArgument(str(e))

    @commands.check_any(common.in_adminlist())
    @commands.command(hidden=False)
    async def disable(self, ctx, command):
        """Disable a enabled command.
		Aliases:
			/disable

		Parametres:
			command: Le nom d'une commande ou d'un alias.

		Exemple:
			/disable roll
		"""
        commande = self.bot.get_command(command)
        if commande is None:
            await ctx.message.add_reaction(u'\U0001F534')
            common.log_command_error(self.logger, ctx, f"Echec lors de la désactivation de la commande '{command}'",
                                     f"La commande '**{command}**' n'a pas été trouvée.")
            raise commands.BadArgument(f"La commande '**{command}**' n'a pas été trouvée.")
        commande.enabled = False
        await ctx.message.add_reaction(u'\U0001F7E2')
        common.log_command(self.logger, ctx, f"Désactivation de la commande '{command}'")

    @commands.check_any(common.in_adminlist())
    @commands.command(hidden=False)
    async def enable(self, ctx, command):
        """Enable a disabled command.
		Aliases:
			/enable

		Parametres:
			command: Le nom d'une commande ou d'un alias.

		Exemple:
			/enable roll
		"""
        commande = self.bot.get_command(command)
        if commande is None:
            await ctx.message.add_reaction(u'\U0001F534')
            common.log_command_error(self.logger, ctx, f"Echec lors de l'activation de la commande '{command}'",
                                     f"La commande '**{command}**' n'a pas été trouvée.")
            raise commands.BadArgument(f"La commande '**{command}**' n'a pas été trouvée.")
        commande.enabled = True
        await ctx.message.add_reaction(u'\U0001F7E2')
        common.log_command(self.logger, ctx, f"Activation de la commande '{command}'")

    @commands.check_any(common.in_adminlist())
    @commands.command(hidden=False)
    async def sudo(self, ctx, member: Union[discord.Member, discord.User] = None, *, command: str = None):
        """Run a command as another user.
		Aliases:
			/sudo

		Parametres:
			membre: Nom/Mention/ID d'un membre discord.
			command: Nom (et parametres) d'une commande de ce bot.

		Exemple:
			/sudo Ikya roll 1d20
		"""
        if member is not None and command is not None:
            message = ctx.message
            message.author = member
            message.content = ctx.prefix + command
            await self.bot.invoke(await self.bot.get_context(message))
            common.log_command(self.logger, ctx, f"Utilisation de sudo\n\tMembers: {member.display_name}\n\tCommand: {command}")
        else:
            await ctx.message.add_reaction(u'\U0001F534')
            common.log_command_error(self.logger, ctx, f"Echec lors de l'utilisation de sudo\n\tMembers: {member}\n\tCommand: {command}",
                                     f"Il manque un ou plusieurs paramètres.")
            raise commands.BadArgument(f"Il manque un ou plusieurs paramètres.")

#TODO
"""
    @commands.check_any(common.in_adminlist())
    @commands.command(hidden=False)
    async def pull(self, ctx, *, message: str = None):
        ""Pull from GitLab.
		Aliases:
			/pull

		Parametres:
			(message): 'reboot' OU un message a envoyer a Kaginor.

		Exemple:
			/pull reboot
		""
        git_dir = getcwd()
        g = Git(git_dir)
        res = (g.pull())
        await ctx.author.send(res)
        if message == 'reboot':
            await self.reboot(ctx)
"""

def setup(bot):
    bot.add_cog(AdminCog(bot))
