from discord.ext import commands
import discord


def on_userlist(filepath, userid):
    with open(filepath, 'r') as fp:
        for cnt, line in enumerate(fp):
            if userid == int(line):
                return True
    return False

def in_adminlist(*, check=all, **perms):
    async def adminlist(ctx):
        if on_userlist("data/userlist/admin.uil", ctx.author.id):
            return True
        return False

    return commands.check(adminlist)
