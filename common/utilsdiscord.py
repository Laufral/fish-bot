from common import logger
import discord


def search_member(guild, elem):
    member = discord.utils.get(guild.members, mention=elem)
    if member is not None:
        return member

    member = discord.utils.get(guild.members, display_name=elem)
    if member is not None:
        return member

    member = discord.utils.get(guild.members, name=elem)
    if member is not None:
        return member

    member = discord.utils.get(guild.members, id=elem)
    if member is not None:
        return member

    elem = elem.replace('!', '')
    member = discord.utils.get(guild.members, mention=elem)
    if member is not None:
        return member

    try:
        elem = elem.split('#')
        member = discord.utils.get(guild.members, name=elem[0], discriminator=elem[1])
    except:
        return None

    return member


def search_role(guild, elem):
    role = discord.utils.get(guild.roles, mention=elem)
    if role is not None:
        return role
    role = discord.utils.get(guild.roles, name=elem)
    if role is not None:
        return role
    return discord.utils.get(guild.roles, id=elem)

def filter_on_member_update(before, after, *, status=False, activity=False, nickname=False, roles=False, pending=False):
    ret = False
    if before.status != after.status and status:
        ret = True
    if before.activity != after.activity and activity:
        ret = True
    if before.nick != after.nick and nickname:
        ret = True
    if before.roles != after.roles and roles:
        ret = True
    if before.pending != after.pending and pending:
        ret = True
    return ret

async def send_help(ctx, _logger):
    await ctx.channel.send(f"```\n{ctx.command.help}\n```")
    if _logger is not None:
        logger.log_command(_logger, ctx, ctx.command.help)

async def sendFailureEmoji(ctx):
    await ctx.message.add_reaction(u'\U0001F534')

async def sendSuccesEmoji(ctx):
    await ctx.message.add_reaction(u'\U0001F7E2')
