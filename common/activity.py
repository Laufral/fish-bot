import discord

def getNewActivityFromString(status, name, url = ''):
    if status == 'playing':
        activity = discord.Game(name=name)
    elif status == 'listening':
        activity = discord.Activity(name=name, type=discord.ActivityType.listening)
    elif status == 'streaming':
        activity = discord.Streaming(name=name, type=discord.ActivityType.streaming, url=url)
    elif status == 'watching':
        activity = discord.Activity(name=name, type=discord.ActivityType.watching)
    elif status == 'competing':
        activity = discord.Activity(name=name, type=discord.ActivityType.competing)
    #elif status == 'custom':
    #    activity = discord.Activity(name=name, type=discord.ActivityType.custom)
    else:
        activity = discord.Activity(name=name, type=discord.ActivityType.unknown)
    return activity
