import configparser as ConfigParser
import os
from os.path import dirname

import watchdog as watchdog
from watchdog.observers import Observer

class Options(watchdog.events.FileSystemEventHandler):
    """watchdog permet de voir quand on modifie le fichier config"""
    REQUIRED_SECTIONS = (
        'credentials'
    )

    def __init__(self, config_file, *args, **kwargs):
        self._config = ConfigParser.RawConfigParser()
        self.config_file = config_file
        self.reload_config()
        self._config_valid = False
        self.observer = Observer()
        self.observer.schedule(self, path='.', recursive=False)
        self.observer.start()
        self.event_listeners = []

    # TODO A TESTER
    def add_event_listeners(self, event_listener):
        if not self.event_listeners.index(event_listener):
            self.event_listeners.append(event_listener)

    def remove_event_listeners(self, event_listener):
        if self.event_listeners.index(event_listener):
            self.event_listeners.remove(event_listener)

    def __del__(self):
        self.observer.stop()
        self.observer.join()

    def on_any_event(self, event):
        if self.config_file in event.src_path:
            self.reload_config()
            """for listener in self.event_listeners:
                listener(event)"""

    def reload_config(self):
        self._config.read(self.config_file)
        self.check_config()

    def check_config(self):
        self._config_valid = False

        try:
            for section in self.REQUIRED_SECTIONS:
                self._config.items(section)
            self._config_valid = True
        except ConfigParser.NoSectionError:
            self._config_valid = False

    def __getitem__(self, section):
        #TODO faire ça plus propre
        a = self._config.items(section)
        elm_to_conv = []
        for elm in a:
            try:
                int(elm[1])
                elm_to_conv.append(elm[0])
            except (Exception) as e:
                pass
        b = dict(a)
        for elm in elm_to_conv:
            b[elm] = int(b[elm])

        return b

    @property
    def valid(self):
        return self._is_config_valid
